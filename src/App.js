
import './App.css';
import Ingredients from './components/Ingredients/Ingredients';

function App() {
  return (
    
  <div>
    <Ingredients />
  </div>
  );
}

export default App;
